/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class CastTest {

    public CastTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getInt() method, of class "Cast".
     */
    @Test
    public void testGetInt() {
        System.out.println("getInt()");

        String n      = "";
        int expResult = 0;
        int result    = Cast.getInt(n);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        n         = "as34";
        expResult = 0;
        result    = Cast.getInt(n);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        n         = "34as";
        expResult = 0;
        result    = Cast.getInt(n);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        n         = "34";
        expResult = 34;
        result    = Cast.getInt(n);
        assertEquals(expResult, result);
    }

} // class
