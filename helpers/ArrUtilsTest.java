/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class ArrUtilsTest {

    private ArrayList<String> arrList;
    private final Map<String, String> map = new LinkedHashMap<>();
    private final String[] arr = { 
        "orange", 
        "apple", 
        "apple", 
        "banana", 
        "grape", 
        "apple", 
        "lemon"
    };


    public ArrUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    @SuppressWarnings("empty-statement")
    public void setUp() {
        String[] strs = {"Foo", "Baz", "Dom", "Maz"};
        this.arrList  = new ArrayList<>(Arrays.asList(strs));

        map.put("one",   "Foo");
        map.put("two",   "Baz");
        map.put("three", "Dom");
        map.put("four",   "Maz");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of filterRepeatedConsecutiveStrs() method, of class "ArrUtils"
     */
    @Test
    public void testFilterRepeatedConsecutiveStrs() {
        System.out.println("filterRepeatedConsecutiveStrs");
        String[] expResult =
            {"orange", "apple", "banana", "grape", "apple", "lemon"};

        String[] result    = ArrUtils.filterRepeatedConsecutiveStrs(this.arr);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of filterRepeatedStr() method, of class "ArrUtils"
     */
    @Test
    public void testFilterRepeatedStrs() {
        System.out.println("filterRepeatedStr()");

        String[] arrStr    = {"nan", "xxx", "spy", "xxx", "spy"};
        String[] expResult = {"nan", "spy", "xxx"};

        String[] result    = ArrUtils.filterRepeatedStrs(arrStr);
        assertEquals(expResult.length, result.length);

        // Trace.ln("--------------------------\n" + StrUtils.arrToStr(result));
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getKeyFromMap() method, of class "ArrUtils"
     */
    @Test
    public void testGetKeyFromMap() {
        System.out.println("getKeyFromMap()");

        int pos          = 0;
        String expResult = "one";

        String result    = ArrUtils.getKeyFromMap(pos, this.map);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        pos       = 1;
        expResult = "two";

        result    = ArrUtils.getKeyFromMap(pos, map);
        assertEquals(expResult, result);
        
        ////////////////////////////////////////////////////////////////
        pos       = -1;
        expResult = "one";

        result    = ArrUtils.getKeyFromMap(pos, map);
        assertEquals(expResult, result);
    }

    /**
     * Test of getKeyFromMap2() method, of class "ArrUtils"
     */
    @Test
    public void testGetKeyFromMap2() {
        System.out.println("getKeyFromMap2()");

        int pos          = 0;
        String expResult = "one";

        String result    = ArrUtils.getKeyFromMap2(pos, map);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        pos       = 1;
        expResult = "two";

        result    = ArrUtils.getKeyFromMap2(pos, map);
        assertEquals(expResult, result);
        
        ////////////////////////////////////////////////////////////////
        pos       = -1;
        expResult = "one";

        result    = ArrUtils.getKeyFromMap2(pos, map);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getKeyPositionFromMap() method, of class "ArrUtils"
     */
    @Test
    public void testGetKeyPositionFromMap() {
        System.out.println("getKeyPositionFromMap()");
        
        String key    = "two";
        int expResult = 1;
        
        int result    = ArrUtils.getKeyPositionFromMap(key, this.map);
        assertEquals(expResult, result);
    }

    /**
     * Test of getStrPosition() method, of class "ArrUtils"
     */
    @Test
    public void testGetStrPosition_ArrayList_String() {
        System.out.println("getStrPosition()");

        String str    = "Foo";
        int expResult = 0;

        int result    = ArrUtils.getStrPosition(this.arrList, str);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        str       = "Dom";
        expResult = 2;

        result    = ArrUtils.getStrPosition(this.arrList, str);
        assertEquals(expResult, result);
    }

    /**
     * Test of getStrPosition() method, of class "ArrUtils"
     */
    @Test
    public void testGetStrPosition_ObservableList_String() {
        System.out.println("getStrPosition()");
        
        ObservableList<String> obsList = 
            FXCollections.observableList(this.arrList);
        String str    = "Foo";
        int expResult = 0;
        
        int result = ArrUtils.getStrPosition(obsList, str);
        assertEquals(expResult, result);
    }

} // class
