/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class DateUtilsTest {

    public DateUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void xxx() {
        System.out.println("xxx()");
        Trace.ln("---------------------------------------------------------\n");

        String date1 = "21/05/2018"; // 1526860800
        long ts1     = DateUtils.getTimestampFromStrDate(date1);
        String date2 = DateUtils.getStrDateFromTimestamp(ts1);
        long ts2     = DateUtils.getTimestampFromStrDate(date2);

        Trace.ln("---------------------------------------------------------\n");
        Trace.ln("Date 1: " + date1 + " | " + ts1);
        Trace.ln("Date 2: " + date2 + " | " + ts2);
        Trace.ln("---------------------------------------------------------\n");
    }

    /**
     * Test of convertLocaleToStr() method, of class "DateUtils".
     */
    @Test
    public void testConvertLocaleToStr() {
        System.out.println("convertLocaleToStr()");

        // Use a variable date
        LocalDate ld     = LocalDate.now();
        String expResult = DateUtils.convertLocaleToStr2(ld);

        String result    = DateUtils.convertLocaleToStr(ld);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        // Use a fixed date
        String sDate   = "23/08/1999";
        String pattern = "dd/MM/yyyy";
        ld = LocalDate.parse(sDate, DateTimeFormatter.ofPattern(pattern));
        expResult = sDate;

        result    = DateUtils.convertLocaleToStr(ld);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertLocaleToStr2() method, of class "DateUtils".
     */
    @Test
    public void testConvertLocaleToStr2() {
        System.out.println("convertLocaleToStr2()");

        // Use a variable date
        LocalDate ld     = LocalDate.now();
        String expResult = DateUtils.convertLocaleToStr(ld);

        String result    = DateUtils.convertLocaleToStr2(ld);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        // Use a fixed date
        String sDate   = "23/08/1999";
        String pattern = "dd/MM/yyyy";
        ld = LocalDate.parse(sDate, DateTimeFormatter.ofPattern(pattern));
        expResult = sDate;

        result    = DateUtils.convertLocaleToStr2(ld);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertStrToLocale() method, of class "DateUtils".
     */
    @Test
    public void testConvertStrToLocale() {
        System.out.println("convertStrToLocale()");

        String date    = "23/08/1999";
        String pattern = "dd/MM/yyyy";
        LocalDate expResult =
            LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern));

        LocalDate result    = DateUtils.convertStrToLocale(date);
        assertEquals(expResult, result);
    }

    /**
     * Test of formatDate() method, of class "DateUtils".
     */
    @Test
    public void testFormatDate() {
        System.out.println("formatDate()");

        String date1     = "10.12.2017";
        String date2     = "10-12-2017";
        String expResult = "10/12/2017";

        String result1   = DateUtils.formatDate(date1);
        String result2   = DateUtils.formatDate(date2);
        assertEquals(expResult, result1);
        assertEquals(expResult, result2);
    }

    /**
     * Test of getStrDateFromTimestamp() method, of class "DateUtils".
     */
    @Test
    public void testGetStrDateFromTimestamp() {
        System.out.println("getStrDateFromTimestamp()");

        long epoch       = 1530315675;
        String expResult = "30/06/2018";

        String result    = DateUtils.getStrDateFromTimestamp(epoch);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////////////
        epoch     = 1526860800;
        expResult = "21/05/2018";
        result    = DateUtils.getStrDateFromTimestamp(epoch);
        assertEquals(expResult, result);
    }

    /**
     * Test of getStrDatetimeFromTimestamp() method, of class "DateUtils".
     */
    @Test
    public void testGetStrDatetimeFromTimestamp() {
        System.out.println("getStrDatetimeFromTimestamp()");

        long epoch       = 1530315675;
        String expResult = "30/06/2018 01:41";
        String result    = DateUtils.getStrDatetimeFromTimestamp(epoch * 1000);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNow() method, of class "DateUtils".
     */
    @Test
    public void testGetNow() {
        System.out.println("getNow()");

        int expResultLength = 5;
        String result       = DateUtils.getNow(); // Format: "HH:mm"
        assertEquals(expResultLength, result.length( ));
    }

    /**
     * Test of getTimestamp() method, of class "DateUtils".
     */
    @Test
    public void testGetTimestamp() {
        System.out.println("getTimestamp()");

        long expResult = 10;
        long result    = DateUtils.getTimestamp();
        assertEquals(expResult, (result + "").length( ));
    }

    /**
     * Test of getTimestampFromStrDate() method, of class "DateUtils".
     */
    @Test
    public void testGetTimestampFromStrDate() {
        System.out.println("getTimestampFromStrDate()");

        String date    = "30/06/2018";
        long expResult = 1530309600; // GMT: Friday, 29 June 2018 22:00:00
        // My time zone: sábado, 30/06/2018 0:00:00

        long result    = DateUtils.getTimestampFromStrDate(date);
        // Trace.ln("@result: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getToday() method, of class "DateUtils".
     */
    @Test
    public void testGetToday() {
        System.out.println("getToday()");

        long epoch       = DateUtils.getTimestamp();
        String expResult = DateUtils.getStrDateFromTimestamp(epoch);
        String result    = DateUtils.getToday();
        Trace.ln("@expected: " + expResult + "\n@result: " + result); // HACK:
        assertEquals(expResult, result);
    }

    /**
     * Test of getYear() method, of class "DateUtils".
     */
    @Test
    public void testGetYear_0args() {
        System.out.println("getYear()");

        int expResultLength = 4;
        String result       = DateUtils.getYear();
        assertEquals(expResultLength, result.length( ));
    }

    /**
     * Test of getYear() method, of class "DateUtils".
     */
    @Test
    public void testGetYear_long() {
        System.out.println("getYear()");

        long epoch       = DateUtils.getTimestamp();
        String expResult = DateUtils.getYear();
        String result    = DateUtils.getYear(epoch * 1000);
        Trace.ln("@expected: " + expResult + "\n@result: " + result); // HACK:
        assertEquals(expResult, result);
    }

} // class
