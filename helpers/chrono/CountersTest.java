/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers.chrono;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class CountersTest {

    public CountersTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getFromSeconds() method, of class "Counters"
     */
    @Test
    public void testGetFromSeconds() {
        System.out.println("getFromSeconds()");

        int seconds      = -1;
        String expResult = "00:00:00";
        String result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 0;
        expResult = "00:00:00";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 30;
        expResult = "00:00:30";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 60;
        expResult = "00:01:00";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 360;
        expResult = "00:06:00";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 3600;
        expResult = "01:00:00";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 1000;
        expResult = "00:16:40";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 10000;
        expResult = "02:46:40";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        System.out.println("\nFrom here more of 23:59:59 ....................");

        seconds   = 100000;
        expResult = "27:46:40";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 999960;
        expResult = "277:46:00";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);

        seconds   = 1000000;
        expResult = "277:46:40";
        result    = Counters.getFromSeconds(seconds);
        assertEquals(expResult, result);
    }

} // class
