/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers.chrono;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class ChronoTest {

    public ChronoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        System.out.println("Testing reset()...");

        Chrono.reset();
    }

    @After
    public void tearDown() {
        System.out.println("Testing reset()...");

        Chrono.reset();
    }

    /**
     * Test of getCount() method, of class "Chrono"
     */
    @Test
    public void testGetCount() {
        System.out.println("getCount()");

        String expResult = "00:00:01";
        String result    = Chrono.getCount();
        assertEquals(
            "Actual time: " + result,
            expResult,
            result
        );

        ////////////////////////////////////////////////////////////////
        expResult = "00:00:02";
        result    = Chrono.getCount();
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        expResult = "00:00:03";
        result    = Chrono.getCount();
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        System.out.println("\nTesting reset()...");
        Chrono.reset();

        expResult = "00:00:01";
        result    = Chrono.getCount();
        assertEquals(expResult, result);
    }

    /**
     * Test of reset() method, of class "Chrono"
     */
    @Test
    public void testReset() {
        // Note: tested in testGetCount(), setUp() and tearDown() methods
    }

} // class
