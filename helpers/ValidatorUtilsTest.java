/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class ValidatorUtilsTest {

    private String[] content;
    private String errors = "";
    private int numberOfErrorForTest;


    public ValidatorUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.numberOfErrorForTest = 5;
        this.content = new String[this.numberOfErrorForTest];

        System.out.println("addError()");
        for (int i=1; i <= this.numberOfErrorForTest; i++) {
            String err        = "this is the error: " + i;
            this.errors += (err + "\n");
            this.content[(i-1)] = err;
            ValidatorUtils.addError(err);
        }
    }

    @After
    public void tearDown() {
        this.content = null;
        ValidatorUtils.cleanErrors();
    }

    /**
     * Test of addError() method, of class "ValidatorUtils"
     */
    // @Test
    public void testAddError_String() {
        // NOTE: implemented in setUp()
    }

    /**
     * Test of addError() method, of class "ValidatorUtils"
     */
    // @Test
    public void testAddError_String_String() {
        // NOTE: implemented in getErrors()
    }

    /**
     * Test of cleanErrors() method, of class "ValidatorUtils"
     */
    //@Test
    public void testCleanErrors() {
        System.out.println("cleanErrors()");

        // NOTE: implemented in getErrors()
    }

    /**
     * Test of exists() method, of class "ValidatorUtils"
     */
    @Test
    public void testExists() {
        System.out.println("exists()");

        String[] values   = {"454", "fgrt"};
        String[] fields   = {"foo", "bar"};
        boolean result    = ValidatorUtils.exists(values, fields);
        assertTrue(result);

        ////////////////////////////////////////////////////////////////
        values[0] = "";
        result = ValidatorUtils.exists(values, fields);
        assertFalse(result);
    }

    /**
     * Test of getErrors() method, of class "ValidatorUtils"
     */
    @Test
    public void testGetErrors() {
        System.out.println("getStrErrors()");

        List<String> arr = new ArrayList<>();
        arr.addAll(Arrays.asList( this.content ));

        ArrayList<String> expResult = (ArrayList<String>) arr;
        ArrayList<String> result    = ValidatorUtils.getErrors();
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        System.out.println("\nTesting addError()...");

        String err  = "ERR 666";
        String msj  = "Error message";
        String temp = msj + " -> " + err;
        ValidatorUtils.addError(err, msj);
        result      = ValidatorUtils.getErrors();
        expResult.add(temp);
        this.errors += (temp + "\n");
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        System.out.println("\nTesting cleanErrors()...");

        ValidatorUtils.cleanErrors();
        result              = ValidatorUtils.getErrors();
        int expResultNumber = 0;
        int resultNumber    = result.size();
        assertEquals(expResultNumber, resultNumber);
    }

    /**
     * Test of getStrErrors() method, of class "ValidatorUtils"
     */
    @Test
    public void testGetStrErrors() {
        System.out.println("getStrErrors()");


        String expResult = this.errors;
        String result    = ValidatorUtils.getStrErrors();
        assertEquals(expResult, result);
    }

    /**
     * Test of isFutureDate() method, of class "ValidatorUtils"
     */
    @Test
    public void testIsFutureDate() {
        System.out.println("isFutureDate()");

        String date       = "10/10/2010";
        boolean result    = ValidatorUtils.isFutureDate(date);
        assertFalse(result);

        ////////////////////////////////////////////////////////////////
        date   = "10/10/3000";
        result = ValidatorUtils.isFutureDate(date);
        assertTrue(result);
    }

    /**
     * Test of isNumber() method, of class "ValidatorUtils"
     */
    @Test
    public void testIsNumber() {
        System.out.println("isNumber()");

        String str        = "666";
        boolean result    = ValidatorUtils.isNumber(str);
        assertTrue(result);

        ////////////////////////////////////////////////////////////////
        str    = "a12";
        result = ValidatorUtils.isNumber(str);
        assertFalse(result);
    }

    /**
     * Test of isStrSizeBetween() method, of class "ValidatorUtils"
     */
    @Test
    public void testIsStrSizeBetween() {
        System.out.println("isStrSizeBetween()");
        
        String str     = "hola";
        int min        = 5;
        int max        = 10;
        boolean result = ValidatorUtils.isStrSizeBetween(str, min, max);
        assertFalse(result);
        
        ////////////////////////////////////////////////////////////////
        str    = "hola mundo";
        result = ValidatorUtils.isStrSizeBetween(str, min, max);
        assertTrue(result);
        
        ////////////////////////////////////////////////////////////////
        max    = 8;
        result = ValidatorUtils.isStrSizeBetween(str, min, max);
        assertFalse(result);
    }

    /**
     * Test of isTodayDate() method, of class "ValidatorUtils"
     */
    @Test
    public void testIsTodayDate() {
        System.out.println("isTodayDate()");

        String date       = "10/10/2010";
        boolean result    = ValidatorUtils.isTodayDate(date);
        assertFalse(result);

        ////////////////////////////////////////////////////////////////
        date   = "10/10/3000";
        result = ValidatorUtils.isTodayDate(date);
        assertFalse(result);

        ////////////////////////////////////////////////////////////////
        date   = DateUtils.getToday();
        result = ValidatorUtils.isTodayDate(date);
        assertTrue(result);
    }
    
} // class
