/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class NumberDiffTest {

    public NumberDiffTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getSeconds() method, of class "NumberDiff"
     */
    @Test
    public void testGetSeconds() {
        System.out.println("getSeconds()");

        long ts1      = 1479241099;
        long ts2      = 1479241099;
        int expResult = 0;
        int result    = NumberDiff.getSeconds(ts1, ts2);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        ts2       = 1479241199; // + 100
        expResult = 0;
        result    = NumberDiff.getSeconds(ts1, ts2);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        ts2       = 1479241600; // + 501
        expResult = 1;
        result    = NumberDiff.getSeconds(ts1, ts2);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        ts2       = 1479241700; // + 601
        expResult = 1;
        result    = NumberDiff.getSeconds(ts1, ts2);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        ts2       = 1479243099; // + 2000
        expResult = 2;
        result    = NumberDiff.getSeconds(ts1, ts2);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        ts2       = 1479243150; // + 2051
        expResult = 2;
        result    = NumberDiff.getSeconds(ts1, ts2);
        assertEquals(expResult, result);
    }

} // class
