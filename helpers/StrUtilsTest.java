/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class StrUtilsTest {

    private final String[] content = {"hello", "world"};


    public StrUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of arrToStr() method, of class "StrUtils"
     */
    @Test
    public void testArrToStr_ArrayList() {
        System.out.println("arrToStr()");


        List<String> arr = new ArrayList<>();
        arr.addAll(Arrays.asList( this.content ));

        String expResult      = "hello\nworld\n";
        String result         = StrUtils.arrToStr((ArrayList) arr);
        assertEquals(expResult, result);
    }

    /**
     * Test of arrToStr() method, of class "StrUtils"
     */
    @Test
    public void testArrToStr_StringArr() {
        System.out.println("arrToStr()");

        String expResult = "hello\nworld\n";
        String result    = StrUtils.arrToStr( this.content );
        assertEquals(expResult, result);
    }

    /**
     * Test of composeEnumeration() method, of class "StrUtils"
     */
    @Test
    public void testComposeEnumeration() {
        System.out.println("composeEnumeration()");
        
        String[] words = {"foo", "baz", "bar", "koz", "loo"};
        boolean remark = false;
        String expResult = "foo, baz, bar, koz and loo";
        String result = StrUtils.composeEnumeration(words, remark);
        assertEquals(expResult, result);
        
        ////////////////////////////////////////////////////////////////
        remark = true;
        expResult = "\"foo\", \"baz\", \"bar\", \"koz\" and \"loo\"";
        result = StrUtils.composeEnumeration(words, remark);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasContent() method, of class "StrUtils"
     */
    @Test
    public void testHasContent() {
        System.out.println("hasContent()");

        String str     = "";
        boolean result = StrUtils.hasContent(str);
        assertFalse(result); // TRUE

        ////////////////////////////////////////////////////////////////
        str    = "as34fasewd";
        result = StrUtils.hasContent(str);
        assertTrue(result); // FALSE
    }

    /**
     * Test of normalizeSlashes() method, of class "StrUtils"
     */
    @Test
    public void testNormalizeSlashes() {
        System.out.println("normalizeSlashes()");

        String path      = ".\\src\\xxx/aaa///ced/";
        String expResult = "./src/xxx/aaa/ced/";
        String result    = StrUtils.normalizeSlashes(path);
        assertEquals(expResult, result);
    }

    /**
     * Test of replaceAccents() method, of class "StrUtils"
     */
    @Test
    public void testReplaceAccents() {
        System.out.println("replaceAccents()");

        String str       = "áéíóúÁÉÍÓÚ";
        String expResult = "aeiouAEIOU";
        String result    = StrUtils.replaceAccents(str);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        str       = "Giños Übeda";
        expResult = "Ginos Ubeda";
        result    = StrUtils.replaceAccents(str);
        assertEquals(expResult, result);
    }

    /**
     * Test of replaceSpaces() method, of class "StrUtils"
     */
    @Test
    public void testReplaceSpaces() {
        System.out.println("replaceSpaces()");

        String str       = "This is a test: hello word!";
        String newChar   = "@";
        String expResult = "This@is@a@test:@hello@word!";
        String result    = StrUtils.replaceSpaces(str, newChar);
        assertEquals(expResult, result);
    }

    /**
     * Test of ucfirst() method, of class "StrUtils"
     */
    @Test
    public void testUcfirst() {
        System.out.println("ucfirst()");

        String str       = "hello word!";
        String expResult = "Hello word!";
        String result    = StrUtils.ucfirst(str);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        str       = "ümbrella!";
        expResult = "Ümbrella!";
        result    = StrUtils.ucfirst(str);
        Trace.ln("@result: " + result); // HACK:
        assertEquals(expResult, result);
    }

} // class
