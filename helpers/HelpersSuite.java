/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author orici
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
    {
        tasktimer.helpers.ArrUtilsTest.class,
        tasktimer.helpers.CastTest.class,
        tasktimer.helpers.chrono.ChronoSuite.class,
        tasktimer.helpers.DateUtilsTest.class,
        tasktimer.helpers.DirectoryHandlerTest.class,
        tasktimer.helpers.FileHandlerTest.class,
        tasktimer.helpers.FilesTest.class,
        tasktimer.helpers.NumberDiffTest.class,
        tasktimer.helpers.StrUtilsTest.class,
        tasktimer.helpers.ValidatorUtilsTest.class
    }
)
public class HelpersSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

}
