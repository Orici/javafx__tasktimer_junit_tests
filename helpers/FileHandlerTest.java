/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class FileHandlerTest {

    private String testPath;


    public FileHandlerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.testPath = "./test/tasktimer/";
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create() method, of class "FileHandler"
     */
    @Test
    public void testCreate() {
        System.out.println("create()");

        String path    = this.testPath + "xxx/foo.txt";
        boolean result = FileHandler.create(path);
        assertTrue(
            "The file \"" + path + "\" already exist !",
            result
        );
    }

    /**
     * Test of exist() method, of class "FileHandler"
     */
    @Test
    public void testExist() {
        System.out.println("exist()");

        String path    = this.testPath + "zzz/necessary-for-tests.txt";
        boolean result = FileHandler.exist(path);
        assertTrue(
            "The file \"" + path + "\" no exist",
            result
        );

        ////////////////////////////////////////////////////////////////
        path   = this.testPath + "no-exist.xxx";
        result = FileHandler.exist(path);
        assertFalse(result);
    }

    /**
     * Test of getAppPath() method, of class "FileHandler"
     */
    @Test
    public void testGetAppPath() {
        System.out.println("getAppPath()");

        String appDirName = "tasktimer";
        String expResult  = "./src/tasktimer";
        String result     = FileHandler.getAppPath(appDirName);
        result            = StrUtils.normalizeSlashes(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of isAccesible() method, of class "FileHandler"
     */
    @Test
    public void testIsAccesible() {
        System.out.println("isAccesible()");

        String path    = this.testPath + "zzz/necessary-for-tests.txt";
        boolean result = FileHandler.isAccesible(path);
        assertTrue(result);
    }

    /**
     * Test of rename() method, of class "FileHandler"
     */
    @Test
    public void testRename() {
        System.out.println("rename()");

        String path    = this.testPath + "zzz/";
        String name    = "necessary-for-tests.txt";
        String newName = "foozoom.txt";
        boolean result = FileHandler.rename(path, name, newName);
        assertTrue(result);

        ////////////////////////////////////////////////////////////////
        String newPath = path + "foozoom.txt";
        result = FileHandler.exist(newPath);
        assertTrue(
            "The file \"" + newPath + "\" no exist",
            result
        );

        System.out.println("Restoring old file name...");
        result = FileHandler.rename(path, newName, name);
        assertTrue(
            "Failed renaming file: ",
            result
        );
    }

    /**
     * Test of setPath() method, of class "FileHandler"
     */
    @Test
    public void testSetPath() {
        System.out.println("setPath()");

        String path = ".src/steroids/";
        FileHandler.setPath(path);
        String appPath = FileHandler.getAppPath("");
        assertEquals(path, appPath);
    }

} // class
