/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class DirectoryHandlerTest {

    private String testPath;

    public DirectoryHandlerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.testPath = "./test/tasktimer/";
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create() method, of class "DirectoryHandler"
     */
    //@Test
    public void testCreate() {
        System.out.println("create()");

        FileHandler.setPath(this.testPath);
        String name    = "xxx";
        boolean result = DirectoryHandler.create(name);
        assertTrue(
            "Does the dir \"" + this.testPath + name + "\" already exist?",
            result
        );
    }

    /**
     * Test of exist() method, of class "DirectoryHandler"
     */
    @Test
    public void testExist() {
        System.out.println("exist()");

        String path    = this.testPath + "helpers";
        boolean result = DirectoryHandler.exist(path);
        assertTrue(result);

        ////////////////////////////////////////////////////////////////
        path   = this.testPath + "no-exist";
        result = DirectoryHandler.exist(path);
        assertFalse(result);
    }

    /**
     * Test of getFiles() method, of class "DirectoryHandler"
     */
    @Test
    public void testGetFiles() {
        System.out.println("getFiles()");

        String path      = this.testPath + "zzz";
        // Windows paths
        File[] expResult = {
            new File(".\\test\\tasktimer\\zzz\\a-dir"),
            new File(".\\test\\tasktimer\\zzz\\necessary-for-tests.txt")
        };
        File[] result       = DirectoryHandler.getFiles(path);
        int expResultLength = expResult.length;
        int resultLength    = result.length;

        // Trace.ln("@@@ path: " + result[0].getPath( )); // HACK:
        assertEquals(expResultLength, resultLength);
        assertArrayEquals(
            "The paths in expected files have windows dir separators: checks",
            expResult,
            result
        );
    }

    /**
     * Test of isAccesible() method, of class "DirectoryHandler"
     */
    @Test
    public void testIsAccesible() {
        System.out.println("isAccesible()");

        String path    = this.testPath + "helpers";
        boolean result = DirectoryHandler.isAccesible(path);
        assertTrue(result);
    }

    /**
     * Test of readDirs() method, of class "DirectoryHandler"
     */
    @Test
    public void testReadDirs() {
        System.out.println("readDirs()");
        Trace.ln("AppPath: " + DirectoryHandler.getAppPath("tasktimer"));

        String path         = this.testPath + "xxx";
        System.out.println("\nReading: " + path);
        int expResultLength = 0;
        String[] result     = DirectoryHandler.readDirs(path);
        int resultLength    = result.length;
        assertEquals(expResultLength, resultLength);

        ////////////////////////////////////////////////////////////////
        String path2        = this.testPath + "zzz";
        System.out.println("\nReading: " + path2);
        String[] expResult2 = { path2 + "/a-dir" };
        String[] temp       = DirectoryHandler.readDirs(path2);
        String[] result2    = { StrUtils.normalizeSlashes(temp[0]) };
        assertArrayEquals(expResult2, result2);
    }

    /**
     * Test of readFiles() method, of class "DirectoryHandler"
     */
    @Test
    public void testReadFiles() {
        System.out.println("readFiles()");
        Trace.ln("AppPath: " + DirectoryHandler.getAppPath("tasktimer"));

        String path         = this.testPath + "xxx";
        System.out.println("\nReading: " + path);
        int expResultLength = 0;
        String[] result     = DirectoryHandler.readFiles(path);
        int resultLength    = result.length;
        assertEquals(expResultLength, resultLength);

        ////////////////////////////////////////////////////////////////
        String path2        = this.testPath + "zzz";
        System.out.println("\nReading: " + path2);
        String[] expResult2 = { path2 + "/necessary-for-tests.txt" };
        String[] temp       = DirectoryHandler.readFiles(path2);
        String[] result2    = { StrUtils.normalizeSlashes(temp[0]) };
        assertArrayEquals(expResult2, result2);
    }

    /**
     * Test of readFileNames() method, of class "DirectoryHandler"
     */
    @Test
    public void testReadFileNames() {
        System.out.println("readFileNames()");

        String path = this.testPath + "zzz";
        String[] expResult = { "necessary-for-tests" };
        String[] result = DirectoryHandler.readFileNames(path);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of readFileNamesWithExt() method, of class "DirectoryHandler"
     */
    @Test
    public void testReadFileNamesWithExt() {
        System.out.println("readFileNamesWithExt()");

        String path = this.testPath + "zzz";
        String[] expResult = { "necessary-for-tests.txt" };
        String[] result = DirectoryHandler.readFileNamesWithExt(path);
        assertArrayEquals(expResult, result);
    }

} // class
