/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.helpers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class TimeUtilsTest {

    public TimeUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    /**
     * Test of getHumanLongTimeFromHours method, of class TimeUtils.
     */
    @Test
    public void testGetHumanLongTimeFromHours() {
        System.out.println("getHumanLongTimeFromHours()");

        int quantity     = -1;
        String expResult = "0 hours";
        this.assertToHours(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 0;
        expResult = "0 hours";
        this.assertToHours(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 1;
        expResult = "1 hour";
        this.assertToHours(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 5;
        expResult = "5 hours";
        this.assertToHours(quantity, expResult);
    }

    /**
     * Test of getHumanLongTimeFromMinutes() method, of class "TimeUtils"
     */
    @Test
    public void testGetHumanLongTimeFromMinutes() {
        System.out.println("getHumanLongTimeFromMinutes()");

        int quantity     = -1;
        String expResult = "0 minutes";
        this.assertToMinutes(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 0;
        expResult = "0 minutes";
        this.assertToMinutes(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 1;
        expResult = "1 minute";
        this.assertToMinutes(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 5;
        expResult = "5 minutes";
        this.assertToMinutes(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 60;
        expResult = "1 hour, 0 minutes";
        this.assertToMinutes(quantity, expResult); // BUG:

        ////////////////////////////////////////////////////////////////
        quantity  = 61;
        expResult = "1 hour, 1 minute";
        this.assertToMinutes(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 65;
        expResult = "1 hour, 5 minutes";
        this.assertToMinutes(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 120;
        expResult = "2 hours, 0 minutes";
        this.assertToMinutes(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 121;
        expResult = "2 hours, 1 minute";
        this.assertToMinutes(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 137;
        expResult = "2 hours, 17 minutes";
        this.assertToMinutes(quantity, expResult);
        /**/
    }

    /**
     * Test of getHumanLongTimeFromSeconds() method, of class "TimeUtils"
     */
    @Test
    public void testGetHumanLongTimeFromSeconds() {
        System.out.println("getHumanLongTimeFromSeconds()");

        int quantity     = -1;
        String expResult = "0 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 0;
        expResult = "0 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 1;
        expResult = "1 second";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 5;
        expResult = "5 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 60;
        expResult = "1 minute, 0 seconds"; // BUG:
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 61;
        expResult = "1 minute, 1 second";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 62;
        expResult = "1 minute, 2 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3600;
        expResult = "1 hour, 0 minutes, 0 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3601;
        expResult = "1 hour, 0 minutes, 1 second";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3602;
        expResult = "1 hour, 0 minutes, 2 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3660;
        expResult = "1 hour, 1 minute, 0 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3661;
        expResult = "1 hour, 1 minute, 1 second";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3662;
        expResult = "1 hour, 1 minute, 2 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3720;
        expResult = "1 hour, 2 minutes, 0 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3721;
        expResult = "1 hour, 2 minutes, 1 second";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 3722;
        expResult = "1 hour, 2 minutes, 2 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 7200;
        expResult = "2 hours, 0 minutes, 0 seconds";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 7201;
        expResult = "2 hours, 0 minutes, 1 second";
        this.assertToSeconds(quantity, expResult);

        ////////////////////////////////////////////////////////////////
        quantity  = 7202;
        expResult = "2 hours, 0 minutes, 2 seconds";
        this.assertToSeconds(quantity, expResult);
        /**/
    }


    private void assertToHours(int quantity, String expResult) {
        assertEquals(
            expResult, TimeUtils.getHumanLongTimeFromHours(quantity));
    }

    private void assertToMinutes(int quantity, String expResult) {
        assertEquals(
            expResult, TimeUtils.getHumanLongTimeFromMinutes(quantity));
    }

    private void assertToSeconds(int quantity, String expResult) {
        assertEquals(
            expResult, TimeUtils.getHumanLongTimeFromSeconds(quantity));
    }

} // class
