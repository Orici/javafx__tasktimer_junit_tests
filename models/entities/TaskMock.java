/**
 * @file:    TaskMock.java
 * @package: tasktimer.models.entities
 *
 * @proyect: TaskTimer
 * @author:  Moisés Alcocer, 2018
 * @link:    https://www.ironwoods.es
 * @license: http://www.apache.org/licenses/ - Apache License 2.0
 *
 */
package tasktimer.models.entities;


public class TaskMock extends Task {

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration ******************************************/

        /**
         * Constructor
         *
         */
        public TaskMock() {
            super();
        }

        /**
         * Constructor
         *
         * @param name
         * @param description
         * @param proyectId
         */
        public TaskMock(
            String name,
            String description,
            String proyectId
        ) {
            super(name, description, proyectId);
        }


        /**
         *
         * @param obj
         * @return
         */
        @Override
        public boolean equals(Object obj) {
            Task task = (Task) obj;
            return (
                ((this.getId() == null && task.getId() == null) ||
                    this.getId().equals(task.getId( ))) &&
                ((this.getName() == null && task.getName() == null) ||
                    this.getName().equals(task.getName( ))) &&
                ((this.getDescription() == null && task.getDescription() == null) ||
                    this.getDescription().equals(task.getDescription( ))) &&
                ((this.getProjectId() == null && task.getProjectId() == null) ||
                    this.getProjectId().equals(task.getProjectId( ))) &&
                this.getAsignedTime() == task.getAsignedTime() &&
                this.getUsedTime() == task.getUsedTime( ));
        }

        @Override
        public int hashCode() {
            int hash = 3;
            return hash;
        }

} // class
