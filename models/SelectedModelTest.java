/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tasktimer.helpers.Trace;
import tasktimer.models.entities.Project;
import tasktimer.models.entities.Selected;
import tasktimer.models.entities.Task;
import tasktimer.models.entities.TaskMock;

/**
 *
 * @author orici
 */
public class SelectedModelTest {

    private Project project;
    private SelectedModel selModel;
    private String projectId;
    private String taskId;
    private Task task;
    private TaskMock taskMock;


    public SelectedModelTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.selModel = new SelectedModel();

        this.projectId = "2018-001";
        this.project   = new Project("Primer proyecto", "The description");
        this.project.setId(this.projectId);

        this.taskId    = "2018-001-001";
        this.task      =
            new Task("Primera tarea", "The task description", this.projectId);
        this.taskMock  =
            new TaskMock("Primera tarea","The task description",this.projectId);
        this.task.setId(this.taskId);
        this.taskMock.setId(this.taskId);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of get() method, of class "SelectedModel"
     */
    // @Test
    public void testGet() {
        System.out.println("get()");

        Selected result = selModel.get();
        assertEquals(this.project.getId(),   result.getProjectId( ));
        assertEquals(this.project.getName(), result.getProjectName( ));
        assertEquals(this.task.getId(),   result.getTaskId( ));
        assertEquals(this.task.getName(), result.getTaskName( ));
    }

    /**
     * Test of getTask() method, of class "SelectedModel"
     */
    // @Test
    public void testGetTask() {
        System.out.println("getTask()");

        Selected selected = selModel.get();
        Task result       = SelectedModel.getTask(selected);
        // Trace.ln("expResult: " + this.taskMock.toString( )); // HACK:
        // Trace.ln("result: "    + result.toString() + "\n"); // HACK:
        assertTrue(this.taskMock.equals(result));
    }

    /**
     * Test of getProject() method, of class "SelectedModel"
     */
    // @Test
    public void testGetProject() {
        System.out.println("getProject()");

        Selected selected = selModel.get();
        Project expResult = this.project;
        Project result = SelectedModel.getProject(selected);

        assertEquals(expResult.getId(),   result.getId( ));
        assertEquals(expResult.getName(), result.getName( ));
        assertEquals(expResult.getDescription(), result.getDescription( ));
        assertEquals(expResult.getFinalDate(),   result.getFinalDate( ));
        assertEquals(expResult.getAsignedTime(), result.getAsignedTime( ));
        assertEquals(expResult.getUsedTime(), result.getUsedTime( ));
        // assertTrue(expResult.equals(result));
        // assertEquals(expResult, result);
    }

    /**
     * Test of save() method, of class "SelectedModel"
     */
    // @Test
    public void testSave() {
        System.out.println("save()");

        Project project = this.project;
        Task task       = this.task;
        boolean result  = selModel.save(project, task);
        assertTrue(result);
    }

    /**
     * Test of saveCounter() method, of class "SelectedModel"
     */
    @Test
    public void testSaveCounter() {
        System.out.println("saveCounter()");

        int sessionTime = 100;
        boolean result  = selModel.saveCounter(sessionTime);
        assertTrue(result);

        // Add the quantity other time and check again
        Selected selected   = selModel.get();
        int projectUsedTime = selected.getProjectUsedTime();
        selModel.saveCounter(sessionTime);
        selected = selModel.get();
        int expProjectUsedTime = selected.getProjectUsedTime();
        assertEquals(projectUsedTime + sessionTime, expProjectUsedTime);
    }

} // class
