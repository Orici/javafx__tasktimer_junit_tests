/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import tasktimer.JavaFXThreadingRule;

/**
 *
 * @author orici
 */
public class ValidatorTest {

    @Rule
    public JavaFXThreadingRule javafxRule = new JavaFXThreadingRule();
    // This instance allow to use JavaFx classes: load Toast for example

    public ValidatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of checkData method, of class Validator.
     */
    @Test
    public void testCheckData() {
        System.out.println("checkData()");
        String name        = "xxx";
        String description = "yyy";
        String asignedTime = "666";

        boolean result = Validator.checkData(name, description, asignedTime);
        assertTrue(result); // TRUE

        ////////////////////////////////////////////////////////////////
        // NOTE: this test throws an error Toast -> run alone to avoid blockades
        asignedTime = null;
        // result = Validator.checkData(name, description, asignedTime);
        // assertFalse(result); // FALSE
    }

    /**
     * Test of checkEndDate method, of class Validator.
     */
    @Test
    public void testCheckEndDate() {
        System.out.println("checkEndDate()");
        String finalDate = "25/12/2020";

        boolean result   = Validator.checkEndDate(finalDate);
        assertTrue(result); // TRUE

        ////////////////////////////////////////////////////////////////
        // NOTE: this test throws an error Toast -> run alone to avoid blockades
        finalDate = "25/06/2018";
        // result    = Validator.checkEndDate(finalDate);
        // assertFalse(result); // FALSE
    }

    /**
     * Test of checkProjectId method, of class Validator.
     */
    @Test
    public void testCheckProjectId() {
        System.out.println("checkProjectId()");
        String extId   = "2000-001";

        boolean result = Validator.checkProjectId(extId);
        assertTrue(result); // TRUE

        ////////////////////////////////////////////////////////////////
        extId  = "2018-001-001";
        result = Validator.checkProjectId(extId);
        assertFalse(result); // FALSE
    }

    /**
     * Test of checkTaskId method, of class Validator.
     */
    @Test
    public void testCheckTaskId() {
        System.out.println("checkTaskId()");
        String extId   = "2000-001-001";

        boolean result = Validator.checkTaskId(extId);
        assertTrue(result); // TRUE

        ////////////////////////////////////////////////////////////////////////
        extId  = "2017-001-003";
        result = Validator.checkTaskId(extId);
        assertTrue(result); // TRUE

        ////////////////////////////////////////////////////////////////////////
        extId  = "2017-001";
        result = Validator.checkTaskId(extId);
        assertFalse(result); // FALSE

        ////////////////////////////////////////////////////////////////////////
        extId  = "xxx";
        result = Validator.checkTaskId(extId);
        assertFalse(result); // FALSE
    }

} // class
