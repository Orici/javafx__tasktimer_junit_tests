/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services.storeservice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author orici
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    tasktimer.services.storeservice.AccessorTest.class,
    tasktimer.services.storeservice.StoreRoutesTest.class,
    tasktimer.services.storeservice.StoreUtilsTest.class,
    tasktimer.services.storeservice.LoaderTest.class,
    tasktimer.services.storeservice.StoreServiceTest.class
})
public class StoreserviceSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

}
