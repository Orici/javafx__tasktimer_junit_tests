/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services.storeservice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tasktimer.helpers.DateUtils;
import tasktimer.helpers.Trace;

/**
 *
 * @author orici
 */
public class StoreServiceTest {

    private String year;


    public StoreServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.year = DateUtils.getYear();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of generateProjectId() method, of class "StoreService"
     */
    @Test
    public void testGenerateProjectId() {
        System.out.println("generateProjectId()");

        String expResult = this.year + "-" + "002";
        String result    = StoreService.generateProjectId();
        Trace.ln("Expected: " + expResult + " | Generated: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of generateTaskId() method, of class "StoreService"
     */
    @Test
    public void testGenerateTaskId() {
        System.out.println("generateTaskId()");

        String projectId = "2018-001";
        String expResult = "2018-001-009";

        String result    = StoreService.generateTaskId(projectId);
        String failMsg   = "generateTaskId() failt! \n"
            + "expResult: " + expResult + " | result: " + result;
        assertEquals(failMsg, expResult, result);
        ////////////////////////////////////////////////////////////////

        projectId = "2017-001";
        expResult = "2017-001-004";

        result    = StoreService.generateTaskId(projectId);
        failMsg   = "generateTaskId() failt! \n"
            + "expResult: " + expResult + " | result: " + result;
        assertEquals(failMsg, expResult, result);
    }

    /**
     * Test of getLastProjectId() method, of class "StoreService"
     */
    @Test
    public void testGetLastProjectId() {
        System.out.println("getLastProjectId()");

        String expResult = "2018-001";

        String result    = StoreService.getLastProjectId();
        String failMsg   = "testGetLastProjectId() failt! \n"
            + "expResult: " + expResult + " | result: " + result;
        assertEquals(failMsg, expResult, result);
    }

    /**
     * Test of getLastTaskId() method, of class "StoreService"
     */
    @Test
    public void testGetLastTaskId() {
        System.out.println("getLastTaskId()");

        String projectId = "2018-001";
        String expResult = "2018-001-008";

        String result    = StoreService.getLastTaskId(projectId);
        String failMsg   = "testGetLastTaskId() failt! \n"
            + "expResult: " + expResult + " | result: " + result;
        assertEquals(failMsg, expResult, result);
    }

    /**
     * Test of getProjectsDirs() method, of class "StoreService"
     */
    @Test
    public void testGetProjectsDirs() {
        System.out.println("getProjectsDirs()");

        String[] expResult = {
            "./src/tasktimer/data/2017",
            "./src/tasktimer/data/2018"
        };
        String[] result    = StoreService.getProjectDirPaths();

        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getProjectsFileNames() method, of class "StoreService"
     */
    @Test
    public void testGetProjectsFileNames_0args() {
        System.out.println("getProjectsFileNames()");

        String[] expResult = {
            "2017-001@The_first_One.json",
            "2018-001@Primer_proyecto.json"
        };

        String[] result = StoreService.getProjectsFileNames();
        Trace.ln("Retrieved: " + result.length + " file names !");

        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getProjectsFileNames() method, of class "StoreService"
     */
    @Test
    public void testGetProjectsFileNames_int() {
        System.out.println("getProjectsFileNames()");

        int year = 2017;
        String[] expResult = {
            "2017-001@The_first_One.json"
        };

        String[] result = StoreService.getProjectsFileNames(year);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getTasksFileNames() method, of class "StoreService"
     */
    @Test
    public void testGetTasksFileNames() {
        System.out.println("getTasksFileNames()");

        String projectId   = "2017-001";
        String[] expResult = {
            "2017-001-001@Primera_tarea.json",
            "2017-001-003@TestTask_003.json"
        };

        String[] result = StoreService.getTasksFileNames(projectId);
        String failMsg  = "getTasksFileNames() test failt!";

        assertArrayEquals(failMsg, expResult, result);
    }

} // class
