/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services.storeservice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tasktimer.models.entities.Project;
import tasktimer.models.entities.Selected;
import tasktimer.models.entities.Task;

/**
 *
 * @author orici
 */
public class AccessorTest {

    private Project project;
    private String projectId;
    private String taskId;
    private Task task;
    
    private Selected originalSelected; // used to restore "selected.json"

    
    public AccessorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.projectId = "2018-001";
        this.project   = new Project("Primer proyecto", "Description");
        this.project.setId(this.projectId);

        this.taskId = "2018-001-001";
        this.task   = new Task("Primera tarea", "Description", this.projectId);
        this.task.setId(this.taskId);
    }

    @After
    public void tearDown() {
        // Restore selected data
        Accessor.setSelected(this.originalSelected);
    }

    /**
     * Test of existsProject() method, of class "Accessor"
     */
    @Test
    public void testExistsProject() {
        System.out.println("existsProject()");

        Project project = this.project;
        boolean result  = Accessor.existsProject(project);
        assertTrue(result);

        ////////////////////////////////////////////////////////////////
        project.setId("2018-999");
        result = Accessor.existsProject(project);
        assertFalse(result);
    }

    /**
     * Test of hasSelected() method, of class "Accessor"
     */
    @Test
    public void testExistSelectedFile() {
        System.out.println("existSelectedFile()");

        boolean result = Accessor.existSelectedFile();
        assertTrue(result);
    }

    /**
     * Test of getProject() method, of class "Accessor"
     */
    @Test
    public void testGetProject() {
        System.out.println("getProject()");

        Project expResult = this.project;
        Project result    = Accessor.getProject(this.projectId);

        assertNotNull(result);
        assertEquals(expResult.getId(),   result.getId( ));
        assertEquals(expResult.getName(), result.getName( ));
    }

    /**
     * Test of getSelected() method, of class "Accessor"
     */
    @Test
    public void testGetSelected() {
        System.out.println("getSelected()");

        Selected expResult = this.getSelected();
        Selected result = this.originalSelected = Accessor.getSelected();
        assertEquals(expResult.getProjectId(),   result.getProjectId( ));
        assertEquals(expResult.getProjectName(), result.getProjectName( ));
        assertEquals(expResult.getTaskId(),      result.getTaskId( ));
        assertEquals(expResult.getTaskName(),    result.getTaskName( ));
        assertEquals(expResult.getTaskUsedTime(), result.getTaskUsedTime( ));
    }

    /**
     * Test of getTask() method, of class "Accessor"
     */
    @Test
    public void testGetTask() {
        System.out.println("getTask()");

        Task expResult = this.task;
        Task result    = Accessor.getTask(this.taskId);

        assertNotNull(result);
        assertEquals(expResult.getId(),   result.getId( ));
        assertEquals(expResult.getName(), result.getName( ));
    }

    /**
     * Test of setProject() method, of class "Accessor"
     */
    @Test
    public void testSetProject() {
        System.out.println("setProject()");

        Project project  = this.project;
        String expResult = this.projectId;
        String result    = Accessor.setProject(project);
        assertEquals(expResult, result);
    }

    /**
     * Test of setSelected() method, of class "Accessor"
     */
    @Test
    public void testSetSelected() {
        System.out.println("setSelected()");

        Selected selected = null;
        boolean result    = Accessor.setSelected(selected);
        assertFalse(result);

        selected = Selected.getInstance(
            1530316800,
            3600,
            "Una descripción de prueba para el proyecto",
            "2018-001",
            "Primer proyecto",
            100,

            3600,
            "Descripción de la tarea actual",
            "2018-001-001",
            "Primera tarea",
            100
        );
        result   = Accessor.setSelected(selected);
        assertTrue(result);
    }

    /**
     * Test of setTask() method, of class "Accessor"
     */
    @Test
    public void testSetTask() {
        System.out.println("setTask()");


        String expResult = this.taskId;
        String result    = Accessor.setTask(this.task);
        assertEquals(expResult, result);
    }


    private Selected getSelected() {
        return Selected.getInstance(
            1530316800,
            144000,
            "projectDescription",
            "2018-001",
            "Primer proyecto",
            0,
            0,
            "taskDescription",
            "2018-001-001",
            "Primera tarea",
            0
        );
    }

} // class
