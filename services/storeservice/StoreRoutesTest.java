/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services.storeservice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tasktimer.helpers.Trace;

/**
 *
 * @author orici
 */
public class StoreRoutesTest {

    public StoreRoutesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createTaskDirPath() method, of class "StoreRoutes"
     */
    @Test
    public void testCreateProjectPath() {
        System.out.println("createProjectPath()");

        String projectId   = "2018-099";
        String projectName = "Tiki taka";
        String expResult   = "./src/tasktimer/data/2018/099@Tiki_taka.json";

        String result = StoreRoutes.createProjectPath(projectId, projectName);
        assertEquals(expResult, result);
    }

    /**
     * Test of createTaskDirPath() method, of class "StoreRoutes"
     */
    @Test
    public void testCreateTaskDirPath() {
        System.out.println("createTaskDirPath()");

        String projectId = "2017-100";
        String expResult = "./src/tasktimer/data/2017/100/";

        String result    = StoreRoutes.createTaskDirPath(projectId);
        assertEquals("Reult: " + result, expResult, result);
    }

    /**
     * Test of createTaskPath() method, of class "StoreRoutes"
     */
    @Test
    public void testCreateTaskPath() {
        System.out.println("createTaskPath()");

        String extId     = "2099-666-005";
        String taskName  = "The xxx day";
        String expResult = "./src/tasktimer/data/2099/666/005@The_xxx_day.json";

        String result    = StoreRoutes.createTaskPath(extId, taskName);
        Trace.ln("result: " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getActualProyectsPath() method, of class "StoreRoutes"
     */
    @Test
    public void testGetActualProyectsPath() {
        System.out.println("getActualProyectsPath()");

        String expResult = "./src/tasktimer/data/2018/";

        String result    = StoreRoutes.getActualProyectsPath();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataPath() method, of class "StoreRoutes"
     */
    @Test
    public void testGetDataPath() {
        System.out.println("getDataPath()");

        String expResult = "./src/tasktimer/data/";

        String result    = StoreRoutes.getDataPath();
        assertEquals(expResult, result);
    }

    /**
     * Test of getProjectPath() method, of class "StoreRoutes"
     */
    @Test
    public void testGetProjectPath() {
        System.out.println("getProjectPath()");

        String extId     = "2018-001";
        String expResult = "./src/tasktimer/data/2018/001@Primer_proyecto.json";

        String result    = StoreRoutes.getProjectPath(extId);
        Trace.ln("result: " + result);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        // No exist
        extId     = "2066-001";
        expResult = "";
        result    = StoreRoutes.getProjectPath(extId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTaskDirPath() method, of class "StoreRoutes"
     */
    @Test
    public void testGetTaskDirPath() {
        System.out.println("getTaskDirPath()");

        String extId     = "2018-001"; // project ID
        String expResult = "./src/tasktimer/data/2018/001/";
        String result    = StoreRoutes.getTaskDirPath(extId);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        extId     = "2017-001-891"; // task ID
        expResult = "./src/tasktimer/data/2017/001/";
        result    = StoreRoutes.getTaskDirPath(extId);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        // Dir no exist
        extId     = "2617-051-891"; // task ID
        expResult = "";
        result    = StoreRoutes.getTaskDirPath(extId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTaskPath() method, of class "StoreRoutes"
     */
    @Test
    public void testGetTaskPath() {
        System.out.println("getTaskPath()");

        String extTaskId = "2017-001-003"; // task ID
        String expResult = "./src/tasktimer/data/2017/001/003@TestTask_003.json";
        String result    = StoreRoutes.getTaskPath(extTaskId);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        // Dir no exist
        extTaskId = "2017-001-891"; // task ID
        expResult = "";
        result    = StoreRoutes.getTaskPath(extTaskId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSelectedProyectsPath() method, of class "StoreRoutes"
     */
    @Test
    public void testGetSelectedProyectsPath() {
        System.out.println("getSelectedProyectsPath()");

        int year         = 2018;
        String expResult = "./src/tasktimer/data/2018/";
        String result    = StoreRoutes.getSelectedProyectsPath(year);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        year      = 2066;
        expResult = "./src/tasktimer/data/2066/";
        result    = StoreRoutes.getSelectedProyectsPath(year);
        assertEquals(expResult, result);

    }

} // class
