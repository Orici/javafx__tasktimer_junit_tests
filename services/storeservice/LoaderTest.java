/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services.storeservice;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tasktimer.models.entities.Project;
import tasktimer.models.entities.Task;

/**
 *
 * @author orici
 */
public class LoaderTest {

    public LoaderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getProjects() method, of class "Loader"
     *
     * NOTE: test the number of objects loaded no themselves
     *
     * @args: void
     */
    @Test
    public void testGetProjects_0args() {
        System.out.println("getProjects()");

        Project[] result = Loader.getProjects();
        assertEquals(2, result.length);
    }

    /**
     * Test of getProjects() method, of class "Loader"
     * NOTE: test the number of objects loaded no themselves
     *
     * @args: int year
     */
    @Test
    public void testGetProjects_int() {
        System.out.println("getProjects()");

        int year = 2017;
        Project[] result = Loader.getProjects(year);
        assertEquals(1, result.length);

        year = 2018;
        Project[] result2 = Loader.getProjects(year);
        assertEquals(1, result2.length);
    }

    /**
     * Test of getProjectsData() method, of class "Loader"
     */
    @Test
    public void testGetProjectsData() {
        System.out.println("getProjectsData()");

        Map<String, String> expResult = getProjectsMap();
        Map<String, String> result    = Loader.getProjectsData();
        assertEquals(
            "Actual proyects differ of the initial used for this test",
            expResult.size(),
            result.size()
        );
        // HACK: Prints the map contents
        //for (Map.Entry x : result.entrySet( )) {
        //    System.out.println(x.getKey() + ", " + x.getValue( ));
        //}
        //System.out.println("-----------------------------------------");

        assertEquals(expResult, result);
    }

    /**
     * Test of getTasks() method, of class "Loader"
     *
     * NOTE: test the number of objects loaded no themselves
     *
     * @args: String projectExtId
     */
    @Test
    public void testGetTasks() {
        System.out.println("getTasks()");

        String projectExtId = "2017-001";
        Task[] result = Loader.getTasks(projectExtId);
        assertEquals(2, result.length);
    }

    /**
     * Test of getTasksData() method, of class "Loader"
     */
    @Test
    public void testGetTasksData() {
        System.out.println("getTasksData()");

        String projectId              = "2017-001";
        Map<String, String> expResult = getTaksMap();
        Map<String, String> result    = Loader.getTasksData(projectId);
        assertEquals(
            "Actual tasks differ of the initial used for this test",
            expResult.size(),
            result.size()
        );
        assertEquals(expResult, result);
    }

        
    private static Map<String, String> getProjectsMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("2017-001", "The first One");
        map.put("2018-001", "Primer proyecto");

        return map;
    }
    
    private static Map<String, String> getTaksMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("2017-001-001", "Primera tarea");
        map.put("2017-001-003", "TestTask 003");

        return map;
    }
        
} // class
