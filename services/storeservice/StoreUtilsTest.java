/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services.storeservice;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class StoreUtilsTest {

    public StoreUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of composeProjectFilePath() method, of class "StoreUtils"
     */
    @Test
    public void testComposeProjectFilePath() {
        System.out.println("composeProjectFilePath()");

        String name      = "Pepe Pito";
        String id        = "2018-666";
        String expResult = "./data/2018/666@Pepe_Pito.json";
        String result    = StoreUtils.composeProjectFilePath(name, id);
        assertEquals(expResult, result);
    }

    /**
     * Test of extractProjectId() method, of class "StoreUtils"
     */
    @Test
    public void testExtractProjectId() {
        System.out.println("extractProjectId()");

        String name      = "2018-001@xxx";
        String expResult = "2018-001";
        String result    = StoreUtils.extractProjectId(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of extractTaskId() method, of class "StoreUtils"
     */
    @Test
    public void testExtractTaskId() {
        System.out.println("extractTaskId()");

        String name      = "2018-001-001@xxx";
        String expResult = "2018-001-001";
        String result    = StoreUtils.extractTaskId(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of filterDataFiles() method, of class "StoreUtils"
     */
    @Test
    public void testFilterDataFiles() {
        System.out.println("filterDataFiles()");

        String[] files     = {
            "2018-001-001@xxx",
            "2018-001-002@xxx",
            "2018"
        };
        String[] expResult = {
            "2018-001-001@xxx",
            "2018-001-002@xxx"
        };
        String[] result    = StoreUtils.filterDataFiles(files);
        assertArrayEquals(expResult, result);
    }
    
    /**
     * Test of orderMapByValue() method, of class "StoreUtils"
     */
    @Test
    public void testOrderMapByValue() {
        System.out.println("orderMap()");
        
        Map<String, String> map       = this.getMapForTests();
        Map<String, String> expResult = this.getExpectedMapForTests();
        Map<String, String> result    = StoreUtils.orderMapByValue(map);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of prepareFileName() method, of class "StoreUtils"
     */
    @Test
    public void testPrepareFileName() {
        System.out.println("prepareFileName()");

        String name      = "2018-666@The file data of ñüs";
        String expResult = "2018-666@The_file_data_of_nus";
        String result    = StoreUtils.prepareFileName(name);
        assertEquals(expResult, result);
    }

    
    private Map<String, String> getMapForTests() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("001", "aaa");
        map.put("002", "fff");
        map.put("003", "ccc");
        
        return map;
    }

    private Map<String, String> getExpectedMapForTests() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("001", "aaa");
        map.put("003", "ccc");
        map.put("002", "fff");
        
        return map;
    }

} // class
