/*
 * javafx test:
 *
 * http://andrewtill.blogspot.com/2012/10/junit-rule-for-javafx-controller-testing.html
 */
package tasktimer.services;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import tasktimer.JavaFXThreadingRule;
import tasktimer.helpers.DateUtils;

/**
 *
 * @author orici
 */
public class DataExtractorTest {

    private ChoiceBox cb;
    private Map<String, String> map;

    @Rule
    public JavaFXThreadingRule javafxRule = new JavaFXThreadingRule();


    public DataExtractorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        String[] elements = {"foo", "baz", "her"};
        ObservableList<String> items =
            FXCollections.observableArrayList(elements);
        this.cb  = new ChoiceBox(items);

        this.map = new LinkedHashMap<>();
        this.map.put("001", elements[0]);
        this.map.put("002", elements[1]);
        this.map.put("003", elements[2]);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getDate() method, of class "DataExtractor"
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate()");

        String date = "30/12/1999";
        LocalDate localDate = DateUtils.convertStrToLocale(date);

        DatePicker dp    = new DatePicker(localDate);
        String expResult = date;
        String result    = DataExtractor.getDate(dp);
        assertEquals(expResult, result);
        
        ////////////////////////////////////////////////////////////////////////
        dp        = new DatePicker();
        expResult = "";
        result    = DataExtractor.getDate(dp);
        assertEquals(expResult, result);
    }

    /**
     * Test of getElementSelectedPosition() method, of class "DataExtractor"
     */
    @Test
    public void testGetElementSelectedPosition() {
        System.out.println("getElementSelectedPosition()");

        int expResult = 0;
        int result    =
            DataExtractor.getElementSelectedPosition(this.cb, this.map);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        expResult = 2;
        this.cb.getSelectionModel().select(expResult);
        result    = DataExtractor.getElementSelectedPosition(this.cb, this.map);
        assertEquals(expResult, result);
    }

    /**
     * Test of getElementSelectedId() method, of class "DataExtractor"
     */
    @Test
    public void testGetElementSelectedId() {
        System.out.println("getElementSelectedId()");

        String expResult = "001";
        String result   = DataExtractor.getElementSelectedId(this.cb, this.map);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        this.cb.getSelectionModel().select(1);
        expResult = "002";
        result    = DataExtractor.getElementSelectedId(this.cb, this.map);
        assertEquals(expResult, result);
    }

} // class
