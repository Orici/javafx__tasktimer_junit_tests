/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services.gui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.paint.Color;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tasktimer.helpers.DateUtils;

/**
 *
 * @author orici
 */
public class TimeControlTest {

    public TimeControlTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getColorForExpiredTime() method, of class "TimeControl"
     */
    @Test
    public void testGetColorForExpiredTime() {
        System.out.println("getColorForExpiredTime()");

        Color expResult = getExpiredColor();

        Color result = TimeControl.getColorForExpiredTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFinalDateColor() method, of class "TimeControl"
     */
    @Test
    public void testGetFinalDateColor() {
        System.out.println("getFinalDateColor()");

        long now        = DateUtils.getTimestamp();
        long finalDate  = now + 1;
        Color expResult = getDefaultColor();

        Color result = TimeControl.getFinalDateColor(finalDate);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        finalDate = 0;

        result = TimeControl.getFinalDateColor(finalDate);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        finalDate = now;
        expResult = getExpiredColor();

        result = TimeControl.getFinalDateColor(finalDate);
        assertEquals(expResult, result);

        ////////////////////////////////////////////////////////////////
        finalDate = now - 1;

        result = TimeControl.getFinalDateColor(finalDate);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataColor() method, of class "TimeControl"
     */
    @Test
    public void testGetDataColor() {
        System.out.println("getDataColor()");

        int asigned     = 0;
        int used        = 0;
        Color expResult = getDefaultColor();

        Color result = TimeControl.getDataColor(asigned, used);
        assertEquals(expResult, result);
    }


    private Color getDefaultColor() {
        return Color.web("#000000");
    }

    private Color getExpiredColor() {
        return Color.web("#b30000");
    }

} // class
