/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services.gui;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orici
 */
public class ChoiceBoxesTest {
    
    public ChoiceBoxesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getItemsObsList() method, of class "ChoiceBoxes"
     */
    @Test
    public void testGetItemsObsList() {
        System.out.println("getItemsObsList()");
        
        String type = "xxx";
        Map<String, String> itemsMap = getMockeMap();
        ObservableList<String> expResult = getMockedObservableList();
        ObservableList<String> result = ChoiceBoxes.getItemsObsList(itemsMap, type);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSelectedItemId() method, of class "ChoiceBoxes"
     */
    @Test
    public void testGetSelectedItemId() {
        System.out.println("getSelectedItemId()");
        
        Number selectedIndex = 0;
        Map<String, String> map = getMockeMap();
        String expResult = "one";
        String result = ChoiceBoxes.getSelectedItemId(selectedIndex, map);
        assertEquals(expResult, result);
        
        ////////////////////////////////////////////////////////////////////////
        selectedIndex = 1;
        expResult     = "two";
        result        = ChoiceBoxes.getSelectedItemId(selectedIndex, map);
        assertEquals(expResult, result);
        
        ////////////////////////////////////////////////////////////////////////
        selectedIndex = 5;
        expResult     = "one";
        result        = ChoiceBoxes.getSelectedItemId(selectedIndex, map);
        assertEquals(expResult, result);
    }
    
    
    private static Map<String, String> getMockeMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("one", "foo");
        map.put("two", "baz");

        return map;
    }

    private ObservableList<String> getMockedObservableList() {
        String[] arrStrs = {"foo", "baz"};
        List<String> testList = Arrays.asList(arrStrs);
        ObservableList<String> result = FXCollections.observableList(testList);
        
        return result;
    }
}
