/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasktimer.services;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tasktimer.models.entities.Project;
import tasktimer.models.entities.Task;

/**
 *
 * @author orici
 */
public class DataCheckerTest {

    public DataCheckerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of checkNecessaryEntities() method, of class "DataChecker"
     */
    @Test
    public void testCheckNecessaryEntities() {
        System.out.println("checkNecessaryEntities()");

        Project p        = new Project();
        Task t           = new Task();
        String appWindow = "One App screen";
        boolean result   = DataChecker.checkNecessaryEntities(p, t, appWindow);
        assertTrue(result);
    }

} // class
