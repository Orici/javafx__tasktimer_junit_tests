package tasktimer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tasktimer.helpers.Trace;
import tasktimer.models.entities.Task;
import tasktimer.services.storeservice.Accessor;
import tasktimer.services.storeservice.Loader;

/**
 *
 * @author orici
 */
public class StuffTest {

    public StuffTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

        
    /**
     * Test of process to add used time to the tasks of a project
     */
    @Test
    public void testGetTasks2() {
        String projectId = "2017-001";
        System.out.println("getTask() - Tasks of project with ID: " + projectId);
        
        
        Task[] arrTasks = Loader.getTasks( projectId );
        for (Task tempTask : arrTasks) {
            int usedTime = tempTask.getUsedTime();
            Trace.ln("" + usedTime);
            
            tempTask.setUsedTime(usedTime + 10);
            Accessor.setTask(tempTask);
        }
        
        System.out.println("*************************************************");
        Task[] arrTasks2 = Loader.getTasks( projectId );
        for (Task tempTask : arrTasks2) {
            int usedTime = tempTask.getUsedTime();
            Trace.ln("" + usedTime);
            
            tempTask.setUsedTime(usedTime + 10);
            Accessor.setTask(tempTask);
        }
        
        System.out.println("*************************************************");
        Task[] arrTasks3 = Loader.getTasks( projectId );
        for (Task tempTask : arrTasks3) {
            int usedTime = tempTask.getUsedTime();
            Trace.ln("" + usedTime);
        }
    }


} // class
